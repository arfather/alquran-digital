<?php

$ENV = file_get_contents("./config.json");
$config = json_decode($ENV, true);
$strJsonFileContents1 = file_get_contents($config['SERVER']."surah/semuaSurah.json");
$data1 = json_decode($strJsonFileContents1, true);
$dataSurahSelected = [];
foreach ($data1["surah"] as $key => $value) {
  if($value["nosurat"] == $_GET['s']){
    array_push($dataSurahSelected, $value);
  }
}
$pagination = false;
$page = 1;
if (isset($_GET['page'])) {
  $page = $_GET['page'];
}
?>

<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <title>Baca Surah</title>
  <style media="screen">

  /*
  bg-primary : #222528
  bg-secondary : #262427
  text-primary : #BEBFC0
  text-secondary : #FFFFFF
  */
  body{
    margin: 0px;
    font-family: "Montserrat", "Helvetica Neue", Arial, sans-serif !important;
  }
  .bg-style {
    background-color: #e3e3e2;
  }
  .bg-primary {
    background-color: #FBFBFA;
  }
  .text-primary {
    color: #000;
  }
  .bg-primary-hover:hover{
    background: #ffffff;
  }

  .bg-secondary {
    box-shadow: 0px 0px 2px 0px #0000003b;
    background-color: #f6f6f667;
  }

  .bg-secondary-active {
    background-color: #272E35;
  }
  .text-secondary {
    color: #000;
  }

  .bg-secondary-hover:hover{
    background: #68c8d512;
  }

  .card:hover{
    box-shadow: 0px 0px 0px 1px #b4bdbf29;
  }
  .card.bg-primary-hover.active{
    background: #ffffff;
    box-shadow: 0px 0px 0px 1px #b4bdbf29;
  }

  .blue{
    background: #68c8d5;
  }

  /* width */
  ::-webkit-scrollbar {
    width: 5px;
  }

  /* Track */
  ::-webkit-scrollbar-track {
    box-shadow: inset 0 0 5px grey;
    border-radius: 20px;
  }

  /* Handle */
  ::-webkit-scrollbar-thumb {
    background: #68c8d5;
    border-radius: 10px;
  }

  /* Handle on hover */
  ::-webkit-scrollbar-thumb:hover {
    background: #000;
  }
  .content{
    display: flex;
    flex-wrap: wrap;
    justify-content: left;
    align-items: left;
  }
  .card{
    padding: 25px;
    box-shadow: rgb(0 0 0 / 10%) 0px 0px 8px -3px, rgb(0 0 0 / 5%) 0px 4px 6px -2px;
    cursor: pointer;
    margin-bottom: 1px
  }

  .w1{
    width: 78%;
  }
  .w2{
    width: 20%;
    padding: 10px;
  }
  .daftarSurah{
    padding-left: 20px;
    max-height: 78vh;
    overflow-y: scroll;
  }
  .daftarSurah-item{
    cursor: pointer;
    font-size: 12px;
    padding: 10px;
  }


  @media only screen and (max-width: 1000px) {
    .card{
      padding: 10px;
      box-shadow: rgb(0 0 0 / 10%) 0px 0px 8px -3px, rgb(0 0 0 / 5%) 0px 4px 6px -2px;
      margin-left: 10px;
      margin-bottom: 10px;
      width: 97%;

      cursor: pointer;
    }
    .w1{
      width: 97%;
    }
    .w2{
      display: none;
    }
  }
  @media only screen and (max-width: 600px) {
    .card{
      padding: 10px;
      box-shadow: rgb(0 0 0 / 10%) 0px 0px 8px -3px, rgb(0 0 0 / 5%) 0px 4px 6px -2px;
      margin-left: 10px;
      margin-bottom: 10px;
      width: 96%;

      cursor: pointer;
    }
    .w1{
      width: 96%;
    }
    .w2{
      display: none;
    }
  }
  .nav{
    box-shadow: rgba(0, 0, 0, 0.04) 0px 20px 25px -5px, rgba(0, 0, 0, 0.04) 0px 10px 10px -5px;
    min-height: 60px;
    margin-bottom: 30px;
    color: #fff;
    font-size: 20px;
    text-align: center;
    justify-content: center;
    align-items: center;
    display: flex;
    flex-wrap: wrap;
    font-weight: bold;
  }
  .text-arab{
    text-align: right;
    font-size: 30px;
    font-family: cursive;
    margin-bottom: 25px;
    margin-left: 100px;
  }
  .indikator{
    font-size: small;
    background: #c4c4c46b;
    max-width: 50px;
    text-align: center;
    padding: 5px;
    border-radius: 10px;
  }
  .color-white{
    color: #fff
  }
  .player{
    font-size: 12px;
    text-align: center;
    background: #68c8d5;
    padding: 5px;
    border-radius: 10px;
    width: 41px;
    margin-top: 10px;
  }
  .box-terjemah{
    margin-left: 100px;
    font-size: 13px;
  }
  .daftarSurah-item.bg-secondary-active{
    background: #68c8d512;
    border-left-width: 3px;
    border-left-style: double;
    border-left-color: #68c8d5;
  }
  .player .tooltiptext {
    color: #000;
    visibility: hidden;
    width: 80px;
    text-align: center;
    border-radius: 6px;
    padding: 5px 0;
    position: absolute;
    z-index: 1;
    bottom: 70%;
    left: 92%;
    margin-left: -60px;
    opacity: 0;
    transition: opacity 0.3s;
  }

  .player .tooltiptext::after {
    content: "";
    position: absolute;
    top: 100%;
    left: 50%;
    margin-left: -5px;
    border-width: 5px;
    border-style: solid;
    border-color: #555 transparent transparent transparent;
  }

  .player:hover .tooltiptext {
    visibility: visible;
    opacity: 1;
  }
  .icon{
    width: 35px;
    margin-left: 10px;
    margin-right: 10px;
    filter: brightness(5.5);
  }
  .stylingWeb{
    box-shadow: rgb(0 0 0 / 10%) 0px 0px 8px -3px, rgb(0 0 0 / 5%) 0px 4px 6px -2px;
    position: fixed;
    bottom: 10px;
    left: 10px;
    background: #eff4f8;
    color: #0000008c;
    width: 50px;
    padding: 10px;
    font-weight: bold;
    border-radius: 10px;
    cursor: pointer;
    text-align: center;
  }

  .stylingWeb-item.active {
    box-shadow: rgb(0 0 0 / 10%) 0px 0px 8px -3px, rgb(0 0 0 / 5%) 0px 4px 6px -2px;
    border-radius: 29px;
    padding-left: 20px;
    padding-right: 20px;
    padding-bottom: 20px;
    z-index: 1;
    position: fixed;
    bottom: 60px;
    left: 20px;
    width: 50vh;
    min-height: 10vh;
  }
  .flex{
    display: flex;
    flex-wrap: wrap;
  }
  .mt-10 {
    margin-top: 10px
  }
  .ml-20 {
    margin-left: 20px
  }
  .mr-10 {
    margin-right: 10px
  }
  .ml-10 {
    margin-left: 10px
  }
  .label{
    font-weight: bold;
    margin-top: 20px;
  }
  button {
    background-color: Transparent;
    background-repeat:no-repeat;
    border: none;
    cursor:pointer;
    overflow: hidden;
    outline:none;
    padding-left: 15px;
    padding-right: 15px;
    padding-top: 4px;
    padding-bottom: 4px;
  }
  button:hover {
    background: #808080;
    padding-left: 15px;
    padding-right: 15px;
    padding-top: 4px;
    padding-bottom: 4px;
    border-radius: 10px;
  }
  button.plus{
    border-radius: 10px;
    border: 1px solid;
  }
  .backdrop{
    top: 0;
    width: 100%;
    height: 100vh;
    position: fixed;
    background: #00000052;
    z-index: 1;
  }
  .tools{
    margin-bottom: 20px;
  }
  </style>
</head>
<body class="bg-primary text-primary">
  <div class="nav blue" style="cursor: pointer"  onclick="goBack()">
    <img src="img/alquran.png" class="icon" alt="alquranIcon">
    Al Quran Digital
    <img src="img/alquran.png" class="icon" alt="alquranIcon">
  </div>
  <script type="text/javascript">
  function goBack() {
    window.location.replace("<?php echo $config['BASE_URL'] ?>");
  }
  </script>
  <?php
  $strJsonFileContentsSurah = file_get_contents($config['SERVER']."dataSurah.json");
  $dataSurah = json_decode($strJsonFileContentsSurah, true);
  ?>
  <div class="content">
    <div class="backdrop" id='backdrop' onclick="openStylingWeb('close')" style="display: none"></div>
    <div class="stylingWeb" onclick="openStylingWeb('open')">style</div>
    <script type="text/javascript">
    function openStylingWeb(param) {
      if (param == 'open') {
        let stylingWebId = document.getElementById('stylingWebId');
        let backdrop = document.getElementById('backdrop');
        backdrop.style.display = backdrop.style.display == "none" ? "block" : "none";
        stylingWebId.style.display = stylingWebId.style.display == "none" ? "block" : "none";
      }else{
        backdrop.style.display = "none";
        stylingWebId.style.display = "none";
      }
    }
    </script>
    <div class="stylingWeb-item bg-style active" id="stylingWebId" style="display: none">
      <div class="label">
        Theme
      </div>
      <div class="flex mt-10 ml-20">
        <div class="">
          <button type="button" name="button" class="text-secondary" onclick="changeTheme('light')">Light</button>
          <button type="button" name="button" class="text-secondary" onclick="changeTheme('dark')">Dark</button>
          <script type="text/javascript">
          function changeTheme(type) {
            let themeCSS = document.createElement('style')
            themeCSS.setAttribute("id", "styleTheme");
            if (type === 'dark') {
              themeCSS.innerHTML = ".bg-primary{background-color: #222528;}" +
              ".text-primary{color: #BEBFC0;}" +
              ".tools button{color: #fff}"+
              ".player .tooltiptext {color: #fff;}" +
              ".bg-style{ background-color: #272E35; }"+
              ".bg-primary-hover:hover{background: #fffffe08;}" +
              ".bg-secondary{box-shadow: 0px 0px 8px 0px #0000003b; background-color: #262427;}" +
              ".bg-secondary-active{background-color: #272E35;}" +
              ".text-secondary{color: #ffffffe0;}" +
              ".bg-secondary-hover:hover{background: #68c8d512;}" +
              ".card:hover{box-shadow: 0px 0px 0px 1px #b4bdbf29;}" +
              ".card.bg-primary-hover.active{background: #fffffe08; box-shadow: 0px 0px 0px 1px #b4bdbf29;}" +
              ".blue{  background: #68c8d51c;}" +
              document.body.appendChild(themeCSS)
            } else {
              themeCSS.innerHTML = ".bg-primary {" +
              "background-color: #FBFBFA;" +
              "}" +
              ".tools button{color: #000}"+
              ".bg-style{ background-color: #e3e3e2; }"+
              ".text-primary {" +
              "color: #000;" +
              "}" +
              ".bg-primary-hover:hover{" +
              "background: #ffffff;" +
              "}" +
              ".bg-secondary {" +
              "box-shadow: 0px 0px 2px 0px #0000003b;" +
              "background-color: #f6f6f667;" +
              "}" +
              ".bg-secondary-active {" +
              "background-color: #272E35;" +
              "}" +
              ".text-secondary {" +
              "color: #000;" +
              "}" +
              ".bg-secondary-hover:hover{" +
              "background: #68c8d512;" +
              "}" +
              ".card:hover{" +
              "box-shadow: 0px 0px 0px 1px #b4bdbf29;" +
              "}" +
              ".card.bg-primary-hover.active{" +
              "background: #ffffff;" +
              "box-shadow: 0px 0px 0px 1px #b4bdbf29;" +
              "}" +
              ".blue{" +
              "background: #68c8d5;" +
              "}"
              document.body.appendChild(themeCSS)
            }
          }


          </script>
        </div>
      </div>
      <div class="label">
        Font size
      </div>
      <div class="flex mt-10 ml-20">
        <div class="">
          <button type="button"  class="text-secondary" name="button" id="minusArabic" onclick="handleFont('arabic', '-')">-</button>
          <span class="mr-10 ml-10">Arabic</span>
          <button type="button"  class="text-secondary" name="button" id="plusArabic" onclick="handleFont('arabic', '+')">+</button>
        </div>
        <div style="margin-left: 20px;">
          <button type="button"  class="text-secondary" name="button" id="minusLatin" onclick="handleFont('terjemah', '-')">-</button>
          <span class="mr-10 ml-10">Terjemah</span>
          <button type="button"  class="text-secondary" name="button" id="plusLatin" onclick="handleFont('terjemah', '+')">+</button>
        </div>
        <script type="text/javascript">
        var fontArab = 30;
        var fontLatin = 13;
        function handleFont(type, action){
          // set disabled button
          let ButtonPlusFontArab = document.getElementById('plusArabic')
          let ButtonMinusFontArab = document.getElementById('minusArabic')
          let ButtonPlusFontLatin = document.getElementById('plusLatin')
          let ButtonMinusFontLatin = document.getElementById('minusLatin')

          // for delete elemen datas
          let styleFontArab = document.getElementById('styleFontArab')
          let styleFontLatin = document.getElementById('styleFontLatin')

          if (type === 'arabic') {

            // remove element
            if (styleFontArab) {
              styleFontArab.remove()
            }

            // sum or minus
            if (action === '-') {
              fontArab = Number(fontArab - 10)
            } else {
              fontArab = Number(fontArab + 10)
            }

            // disabled button
            if (fontArab === 50) {
              ButtonPlusFontArab.disabled = true;
              ButtonPlusFontArab.style.cursor = "not-allowed"

            }else if (fontArab === 30) {
              ButtonMinusFontArab.disabled = true;
              ButtonMinusFontArab.style.cursor = "not-allowed"

            }else{
              ButtonPlusFontArab.disabled = false;
              ButtonMinusFontArab.disabled = false;
              ButtonPlusFontArab.style.cursor = "pointer"
              ButtonMinusFontArab.style.cursor = "pointer"
            }

            let editCSS = document.createElement('style')
            editCSS.setAttribute("id", "styleFontArab");
            editCSS.innerHTML = ".text-arab {font-size: "+fontArab+"px !important;}"
            document.body.appendChild(editCSS)
          } else {
            if (styleFontLatin) {
              styleFontLatin.remove()
            }
            if (action === '-') {
              fontLatin = Number(fontLatin - 2)
            } else {
              fontLatin = Number(fontLatin + 2)
            }

            // disabled button
            if (fontLatin === 21) {
              ButtonPlusFontLatin.disabled = true;
              ButtonPlusFontLatin.style.cursor = "not-allowed"
            }else if (fontLatin === 11) {
              ButtonMinusFontLatin.disabled = true;
              ButtonMinusFontLatin.style.cursor = "not-allowed"
            }else{
              ButtonPlusFontLatin.disabled = false;
              ButtonMinusFontLatin.disabled = false;
              ButtonPlusFontLatin.style.cursor = "pointer"
              ButtonMinusFontLatin.style.cursor = "pointer"

            }

            editCSS = document.createElement('style')
            editCSS.setAttribute("id", "styleFontLatin");
            editCSS.innerHTML = ".text-latin {font-size: "+fontLatin+"px !important;} .text-terjemah {font-size: "+fontLatin+"px !important;}";
            document.body.appendChild(editCSS)
          }
        }
        </script>
      </div>
    </div>
    <div class="w2">

    </div>
    <div class="w2" style="position: fixed" id="parentDaftarSurah">
      <h3 style='text-align: center; margin-block-start: 0;'>Daftar Surah</h3>
      <script type="text/javascript">
      window.onscroll = function(e) {
        if (this.scrollY > 20) {
          document.getElementById('parentDaftarSurah').style.top = "10px";
          document.getElementById('parentDaftarSurah').getElementsByClassName('daftarSurah')[0].style.minHeight = '85vh';
        }else {
          document.getElementById('parentDaftarSurah').style.top = "10%";
          document.getElementById('parentDaftarSurah').getElementsByClassName('daftarSurah')[0].style.minHeight = '78vh';
        }
        // if (Math.ceil(window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
        //   window.location.replace("<?php echo $config['BASE_URL'] ?>"+'baca.php?s=<?php echo $_GET['s']; ?>&page=<?php echo $page+1; ?>&section='+Math.ceil(window.innerHeight + window.scrollY));
        // }
      }
      </script>
      <div class="daftarSurah" style="direction: rtl;">
        <?php
        for ($i=0; $i < count($dataSurah["hasil"]); $i++) { ?>
          <?php if ($_GET['s'] == $i+1) { ?>
            <div style="text-align: left;" class="daftarSurah-item bg-secondary-hover bg-secondary-active bg-secondary text-secondary" onclick="changeSurah('<?php echo $i+1; ?>')">
            <?php }else { ?>
              <div style="text-align: left;" class="daftarSurah-item bg-secondary-hover bg-secondary text-secondary" onclick="changeSurah('<?php echo $i+1; ?>')">
              <?php } ?>

              <?php
              print_r($dataSurah["hasil"][$i]['nama'] . "<br>");
              ?>
            </div>
          <?php } ?>
        </div>
        <script type="text/javascript">
        function changeSurah(param){
          window.location.replace("<?php echo $config['BASE_URL'] ?>"+'baca.php?s='+param);
        }
        </script>
      </div>


      <div class="w1">
        <div class="tools">
          <button type="button" name="button" onclick="playAllAudio()">Play Audio</button>
          <button type="button" name="button" id="hideLatin" onclick="hideLatin()">Hide Latin</button>
          <button type="button" name="button" id="showLatin" style="display: none" onclick="showLatin()">Show Latin</button>

        </div>
        <div id="section-surah">

          <?php foreach ($dataSurahSelected as $key => $value) { ?>
            <section id="surah<?php echo $value["nosurat"]."-".$value["noayat"]; ?>">
                <div class="card bg-secondary text-secondary bg-primary-hover" key="<?php echo $key ?>">
                  <div class="color-white" style="width: 50px;height: 100px;position: absolute;">
                    <div class="indikator">
                      <?php echo $value["nosurat"].":".$value["noayat"]; ?>
                    </div>
                    <div class="player" audioFile="<?php echo $value["mp3"]; ?>" id="player<?php echo $value["noayat"] ?>" onclick="playAudio('<?php echo $value["mp3"]; ?>', '<?php echo $value["noayat"]; ?>', false)">
                      play
                      <span class="tooltiptext bg-primary">Play Audio</span>
                    </div>
                  </div>
                  <div class="text-arab">
                    <?php echo $value["arab"]; ?>
                  </div>
                  <div class="box-terjemah">
                    <div class="text-latin">
                      <?php echo $value["latin"]; ?>
                    </div>
                    <hr>
                    <div class="text-terjemah">
                      <?php echo $value["terjemah"]; ?>
                    </div>
                  </div>
                </div>
              </section>
          <?php } ?>
        </div><br>

        <script type="text/javascript">
        function hideLatin() {
          var all = document.getElementsByClassName('box-terjemah');
          for (var i = 0; i < all.length; i++) {
            all[i].style.display = "none";
          }

          var allArab = document.getElementsByClassName('text-arab');
          for (var i = 0; i < allArab.length; i++) {
            allArab[i].style.textAlign = "right";
          }

          document.getElementById('showLatin').style.display = "inline-block";
          document.getElementById('hideLatin').style.display = "none";

        }
        function showLatin() {
          document.getElementById('showLatin').style.display = "none";
          document.getElementById('hideLatin').style.display = "inline-block";
          var all = document.getElementsByClassName('box-terjemah');
          for (var i = 0; i < all.length; i++) {
            all[i].style.display = "block";
          }

          var allArab = document.getElementsByClassName('text-arab');
          for (var i = 0; i < allArab.length; i++) {
            allArab[i].style.textAlign = "right";
          }

        }
        function gotoSection(surah, ayat) {
          window.location = "<?php echo $config['BASE_URL'] ?>"+'baca.php?s='+surah+'#surah'+surah+"-"+ayat;
        }
        function getUrlVars() {
          var vars = {};
          var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
            if (value.search('#')) {
              vars[key] = value.split("#")[0];
            }else {
              vars[key] = value;
            }
          });
          return vars;
        }
        function playAllAudio() {
          playAudio(document.getElementById('player1').getAttribute('audioFile'), '1', true)
        }
        function playAudio(audioFile, ayat, type) {
          let hoverIndex = ayat - 1;
          let surah = getUrlVars()

          let cardHover = document.getElementsByClassName("card");
          cardHover[hoverIndex].classList.add("active");
          // console.log(cardHover[hoverIndex]);
          let audioDiv = document.querySelector("#playerAudio");
          var audio = document.createElement('audio');
          let currentPlay = document.getElementById('player'+ayat);
          let paramNextAudio = Number(ayat)+1;
          let NextAudio = document.getElementById('player'+paramNextAudio);
          currentPlay.innerHTML = 'pause';
          if (audioDiv) {
            currentPlay.innerHTML = 'play <span class="tooltiptext bg-primary">Play Audio</span>';
            cardHover[hoverIndex].classList.remove("active");
            audioDiv.remove()
          }else {
            audio.setAttribute("id", "playerAudio");
            audio.style.display = "none";
            audio.src = '<?php echo $config["SERVER"] ?>surah/mp3/'+audioFile;
            audio.autoplay = true;
            if (type) {
              gotoSection(surah.s, ayat)
            }
            audio.onended = function(){
              if (type) {
                currentPlay.innerHTML = 'play';
                cardHover[hoverIndex].classList.remove("active");
                audio.remove() //Remove when played.
                if (NextAudio) {
                  playAudio(NextAudio.attributes.audiofile.value, paramNextAudio, true)
                }
              }else {
                currentPlay.innerHTML = 'play';
              }
            };
            document.body.appendChild(audio);
          }
        }
        </script>

      </div>

    </div>
    <script type="text/javascript">
      (function() {
        window.scrollTo(0, <?php echo $_GET['section']-1000 ?>);
      })();
    </script>
  </body>
  </html>
