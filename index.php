<?php
$ENV = file_get_contents("config.json");
$config = json_decode($ENV, true);
?>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Tugas Besar - Arfin Mustofa</title>
  <style media="screen">
  body{
    background: #fafafa;
    margin: 0px;
    font-family: "Montserrat", "Helvetica Neue", Arial, sans-serif !important;
  }
  .content{
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    align-items: center;
  }
  .card{
    padding: 10px;
    box-shadow: rgba(0, 0, 0, 0.1) 0px 10px 15px -3px, rgba(0, 0, 0, 0.05) 0px 4px 6px -2px;
    border-radius: 15px;
    margin-left: 10px;
    margin-bottom: 10px;
    background-color: #FBFBFA;
    color: #619FD3;
    width: 30%;
    height: 75px;
    cursor: pointer;
  }
  .card:hover{
    background: #f2f9f9;
    box-shadow: 0px 0px 0px 1px #CCC;
  }
  @media only screen and (max-width: 1000px) {
    .card{
      padding: 10px;
      box-shadow: rgba(0, 0, 0, 0.1) 0px 10px 15px -3px, rgba(0, 0, 0, 0.05) 0px 4px 6px -2px;
      margin-left: 10px;
      margin-bottom: 10px;
      background-color: #FBFBFA;
      color: blue;
      width: 40%;
      height: 75px;
      cursor: pointer;
    }
  }
  @media only screen and (max-width: 600px) {
    .card{
      padding: 10px;
      box-shadow: rgba(0, 0, 0, 0.1) 0px 10px 15px -3px, rgba(0, 0, 0, 0.05) 0px 4px 6px -2px;
      margin-left: 10px;
      margin-bottom: 10px;
      background-color: #FBFBFA;
      color: blue;
      width: 100%;
      height: 75px;
      cursor: pointer;
    }
  }
  .nav{
    box-shadow: rgba(0, 0, 0, 0.04) 0px 20px 25px -5px, rgba(0, 0, 0, 0.04) 0px 10px 10px -5px;
    min-height: 60px;
    background: #68c8d5;
    margin-bottom: 50px;
    color: #fff;
    font-size: 20px;
    text-align: center;
    justify-content: center;
    align-items: center;
    display: flex;
    flex-wrap: wrap;
    font-weight: bold;
    /* text-transform: uppercase; */
  }
  .namaSurah{
    padding: 15px;
    font-weight: bold;
  }
  .nomorSurah{
    float: right;
    position: relative;
    right: 10px;
    top: 15px;
  }
  .jumlahAyat{
    padding-left: 15px;
    color: #000
  }
  .icon{
    width: 35px;
    margin-left: 10px;
    margin-right: 10px;
    filter: brightness(5.5);
  }
  </style>
</head>
<body>
  <div class="nav">
    <img src="img/alquran.png" class="icon" alt="alquranIcon">
    Al Quran Digital
    <img src="img/alquran.png" class="icon" alt="alquranIcon">
  </div>
  <?php
  $strJsonFileContents1 = file_get_contents($config['SERVER']."dataSurah.json");
  $data1 = json_decode($strJsonFileContents1, true);
  ?>
  <div class="content">
    <?php

    // count($data1["hasil"])
    for ($i=0; $i < count($data1["hasil"]); $i++) { ?>
      <div class="card" onclick="detail('<?php echo $data1["hasil"][$i]['nomor'] ?>')">
        <div class="nomorSurah">
          <?php echo $i+1; ?>
        </div>
        <div class="namaSurah">
          <?php
          print_r($data1["hasil"][$i]['nama'] . " (". $data1["hasil"][$i]['arti'] .")");
          ?>
        </div>
        <div class="jumlahAyat">
          Jumlah Ayat : <?php echo $data1["hasil"][$i]['ayat'] ?>
        </div>
      </div>

    <?php } ?>
    <script type="text/javascript">
      function detail(param){
        window.location.replace("<?php echo $config['BASE_URL'] ?>"+'baca.php?s='+param);
      }
    </script>

  </div>



</body>
</html>
